<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\DiscriminatorColumn;
use Doctrine\ORM\Mapping\DiscriminatorMap;
use Doctrine\ORM\Mapping\InheritanceType;


/**
 * @ORM\Entity
 * @InheritanceType("JOINED")
 * @DiscriminatorColumn(name="discriminator", type="string")
 * @DiscriminatorMap({"BookingObject" = "BookingObject", "Cottage" = "Cottage", "Pension" = "Pension"})
 */
class BookingObject
{
    /**
     * @var int
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=128)
     */
    protected $name_object;


    /**
     * @ORM\Column(type="string", length=256)
     */
    private $full_name;

    /**
     * @ORM\Column(type="string", length=128)
     */
    private $quantity_room;

    /**
     * @ORM\Column(type="decimal", length=128)
     */
    private $price;

    /**
     * @ORM\Column(type="string", length=128)
     */
    private $contact_phone;

    /**
     * @ORM\Column(type="string", length=128)
     */
    private $address;

    public function __toArray(){
        return [
            'id' => $this->getId(),
            'price' => $this->getPrice(),
            'address' => $this->getAddress(),
            'full_name' => $this->getFullName(),
            'name_object' => $this->getNameObject(),
            'contact_phone' => $this->getContactPhone(),
            'quantity_room' => $this->getQuantityRoom()
        ];
    }

    /**
     * @param mixed $name_object
     * @return BookingObject
     */
    public function setNameObject($name_object)
    {
        $this->name_object = $name_object;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getNameObject()
    {
        return $this->name_object;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param mixed $full_name
     * @return BookingObject
     */
    public function setFullName($full_name)
    {
        $this->full_name = $full_name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFullName()
    {
        return $this->full_name;
    }

    /**
     * @param mixed $quantity_room
     * @return BookingObject
     */
    public function setQuantityRoom($quantity_room)
    {
        $this->quantity_room = $quantity_room;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getQuantityRoom()
    {
        return $this->quantity_room;
    }

    /**
     * @param mixed $price
     * @return BookingObject
     */
    public function setPrice($price)
    {
        $this->price = $price;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $contact_phone
     * @return BookingObject
     */
    public function setContactPhone($contact_phone)
    {
        $this->contact_phone = $contact_phone;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getContactPhone()
    {
        return $this->contact_phone;
    }

    /**
     * @param mixed $address
     * @return BookingObject
     */
    public function setAddress($address)
    {
        $this->address = $address;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }


}
