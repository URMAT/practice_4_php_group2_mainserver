<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;



/**
 * @ORM\Entity
 */
class Pension extends BookingObject
{
    /**
     * @var int
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="boolean")
     */
    private $cook;


    /**
     * @ORM\Column(type="boolean")
     */
    private $swimming_pool;

    /**
     * @ORM\Column(type="boolean")
     */
    private $tv;

    /**
     * @return array
     */
    public function __toArray(){
        $pension = array_merge(parent::__toArray());
        $pension['cook'] = $this->getCook();
        $pension['swimming_pool'] = $this->getSwimmingPool();
        $pension['tv'] = $this->getTv();
        return $pension;
    }

    /**
     * @param mixed $cook
     * @return Pension
     */
    public function setCook($cook)
    {
        $this->cook = $cook;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCook()
    {
        return $this->cook;
    }

    /**
     * @param mixed $swimming_pool
     * @return Pension
     */
    public function setSwimmingPool($swimming_pool)
    {
        $this->swimming_pool = $swimming_pool;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSwimmingPool()
    {
        return $this->swimming_pool;
    }

    /**
     * @param mixed $tv
     * @return Pension
     */
    public function setTv($tv)
    {
        $this->tv = $tv;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTv()
    {
        return $this->tv;
    }


}
