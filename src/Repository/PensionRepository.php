<?php

namespace App\Repository;

use App\Entity\Pension;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;


/**
 * @method Pension|null find($id, $lockMode = null, $lockVersion = null)
 * @method Pension|null findOneBy(array $criteria, array $orderBy = null)
 * @method Pension[]    findAll()
 * @method Pension[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PensionRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Pension::class);
    }

    public function sortObject(array $data)
    {
            $qb = $this->createQueryBuilder('a');
                $qb->select('a')
                ->where($qb->expr()->like('a.name_object', ':word'))
                ->setParameter('word', '%'. $data['word'] .'%')
                ->andWhere('a.price BETWEEN :min_price AND :max_price')
                ->setParameter('min_price', $data['min_price'] ?? 0)
                ->setParameter('max_price', $data['max_price'] ?? 10000000)
                ;

            return $qb
                    ->getQuery()
                    ->getResult();

    }
}
